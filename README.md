# Loki Konfiguration für LBANK

Die Zwei Konfigurationen basieren auf der Helm installation der Artefakte.

Dafür ist ein Namespace angelegt worden, und die entsprechenden Artefakte hinzugefügt.

## Prerequisites

Helm Repos hinzufügen und updaten

```
helm repo add grafana https://grafana.github.io/helm-charts
helm repo update
```

Namespace einrichten, sofern noch nicht passiert
```
kubectl apply -f namespace.yaml
```

## Installationen

Die Installationsvalues sind in den entsprechenden Unterordnern abgelegt.

### Grafana Agent
>Unterordner: `grafana-agent`

Installation von Grafana Agent in dem Namespace `lgtm-system`.

```
#vorbereitungen
kubectl apply -f destinationrule.yaml -f agent-config.yaml
#eigentliche installation
helm install grafana-agent grafana/grafana-agent -f values.yaml -n lgtm-system
```

### Loki
>Unterordner: `loki`

Installation von Single Instance Loki in dem Namespace `lgtm-system`.

```
#vorbereitungen
kubectl apply -f authorization.yaml -f loki-datasource.yaml
#eigentliche installation
helm install loki grafana/loki -f values.yaml -n lgtm-system
```

## FAQ
1. Wenn die Loki Installation mit einem PodLogs CRD Fehler abbricht, dann die PodLogs CRD einfach entfernen. ->  `kubectl delete crd podlogs`
