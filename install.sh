#!/bin/bash

helm repo add grafana https://grafana.github.io/helm-charts
helm repo update

# get clusterid and projectid via Rancher api
# For usage it is necessary to create an access token in Rancher under Account and API Keys. The <token> field needs to be replaced by the bearer token and the <rancher_url> with the actual url.
clusterid=$(curl -ks "https://<rancher_url>/v3/clusters?name=mycluster" -H 'content-type: application/json' -H "Authorization: Bearer <token>" |jq -r .data[].id)
projectid=$(curl -ks "https://<rancher_url>/v3/projects?name=System" -H 'content-type: application/json' -H "Authorization: Bearer <token>" | jq -r '.data[] | select(.id | test("local") | not) | .id | split(":")[1]')

#create namespace
cat <<EOF > namespace_created.yaml
apiVersion: v1
kind: Namespace
metadata:
  annotations:
    field.cattle.io/projectId: $clusterid:$projectid
  labels:
    field.cattle.io/projectId: $projectid
  name: lgtm-system
EOF

kubectl apply -f namespace_created.yaml


# install grafana-agent
cd grafana-agent
kubectl apply -f destinationrule.yaml -f agent-config.yaml
helm install grafana-agent grafana/grafana-agent -f values.yaml -n lgtm-system

# install loki
cd ../loki
kubectl apply -f authorization.yaml -f datasource.yaml
helm install loki grafana/loki -f values.yaml -n lgtm-system